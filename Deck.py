import sys

import Card


class Deck:
    def __init__(self, cardSize, cardCount, numberMax):
        self.__m_cardCount = cardCount
        self.__m_cards = self.createDeck(cardSize, numberMax)
        self.__m_cardSize = cardSize
        self.__m_numberMax = numberMax


    def createDeck(self, cardSize, numberMax):
        deck = []
        for i in range(0, self.__m_cardCount):
            deck.append(Card.Card(i + 1, cardSize, numberMax))
        return deck


    def getCardCount(self):
        """Return an integer: the number of cards in this deck"""
        return self.__m_cardCount


    def getCard(self, n):
        """Return card N from the deck"""
        card = None
        n -= 1
        if 0 <= n < self.getCardCount():
            card = self.__m_cards[n]
        return card


    def printDeck(self, file=sys.stdout, idx=None):
        """void function: Print cards from the Deck

        If an index is given, print only that card.
        Otherwise, print each card in the Deck
        """
        if idx is None:
            for idx in range(1, self.__m_cardCount + 1):
                c = self.getCard(idx)
                c.printCard(file)
            print('', file=file)
        else:
            c = self.getCard(idx)
            c.printCard(file)
            print('', file=file)

import random


class NumberSet:
    def __init__(self, size, maxNumber=900):
        """NumberSet constructor"""
        self.__m_size = size
        self.__m_maxNumber = maxNumber
        self.__m_numbers = self.__createNumbers()
        self.__m_nextCount = 0

    def __createNumbers(self):
        numberSet = []
        for i in range(self.__m_size * self.__m_size):
            number = random.randint(0, self.__m_maxNumber)
            while number in numberSet:
                number = random.randint(0, self.__m_maxNumber)
            numberSet.append(number)
        return numberSet

    def get(self, index):
        """Return an integer"""
        if index > len(self.__m_numbers) - 1:
            return None
        else:
            return self.__m_numbers[index]

    def getSize(self):
        """Return an integer: the size of the NumberSet"""
        return self.__m_size

    def setValue(self, index, value):
        """Return an integer: get the number from this NumberSet at an index"""
        self.__m_numbers[index] = value

    def randomize(self):
        """void function: Shuffle this NumberSet"""
        random.shuffle(self.__m_numbers)

    def getNext(self):
        """Return an integer: when called repeatedly return successive values
        from the NumberSet until the end is reached, at which time 'None' is returned"""
        index = self.__m_nextCount
        value = None
        if index < self.__m_size * self.__m_size:
            value = self.__m_numbers[index]
        self.__m_nextCount += 1
        return value

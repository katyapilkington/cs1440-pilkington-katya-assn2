import sys
import math

import NumberSet


class Card:
    def __init__(self, cardId, size, maxNumber=900):
        self.__m_size = size
        self.__m_id = cardId
        self.__m_numbers = NumberSet.NumberSet(size, maxNumber)
        self.__m_card = self.__createCard()


    def __createCard(self):
        self.__m_numbers.randomize()
        card = []
        horizontal = ("+-----" * self.__m_size) + "+"
        index = -1
        for i in range(0, self.__m_size):
            card.append([])
            index += 1
            card[index].append(horizontal)
            card.append([])
            index += 1
            for j in range(0, self.__m_size):
                number = str(self.__m_numbers.getNext())
                spacing = (5 - len(number)) // 2
                square = "|" + (" " * spacing) + number + (" " * spacing)
                if len(square) < 6:
                    square = square + " "
                card[index].append(square)
            card[index].append("|")
        card.append([])
        card[len(card) - 1].append(horizontal)
        if self.__m_size % 2 == 1:
            card[math.floor(len(card) / 2)][math.floor(len(card[1]) / 2) - 1] = "|Free!"
        return card

    def getId(self):
        """Return an integer: the ID number of the card"""
        return self.__m_id


    def getSize(self):
        """Return an integer: the size of one dimension of the card.
        A 3x3 card will return 3, a 5x5 card will return 5, etc.
        """
        return self.__m_size


    def printCard(self, file=sys.stdout):
        """void function:
        Prints a card to the screen or to an open file object"""
        file.write("Card " + str(self.__m_id) + "\n")
        for row in range(0, len(self.__m_card)):
            for column in range(0, len(self.__m_card[row])):
                file.write(self.__m_card[row][column])
            file.write("\n")
        file.write("\n")

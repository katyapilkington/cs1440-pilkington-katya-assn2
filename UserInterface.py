import Deck
import Menu


class UserInterface:
    def __init__(self):
        self.__m_currentDeck = []
        pass

    def run(self):
        """Present the main menu to the user and repeatedly prompt for a valid command"""
        print("Welcome to the Bingo! Deck Generator\n")
        menu = Menu.Menu("Main")
        menu.addOption("C", "Create a new deck")

        keepGoing = True
        while keepGoing:
            command = menu.show()
            if command == "C":
                self.__createDeck()
            elif command == "X":
                keepGoing = False

    def __createDeck(self):
        """Command to create a new Deck"""

        cardSize = self.__getNumberInput("Enter the size of the cards in the deck", 3, 15)
        maxNumber = self.__getNumberInput("Enter the maximum value for a space on a card", cardSize * cardSize * 2,
                                          cardSize * cardSize * 4)
        cardCount = self.__getNumberInput("Enter the number of cards the deck should hold", 3, 1000)

        self.__m_currentDeck = Deck.Deck(cardSize, cardCount, maxNumber)
        self.__deckMenu()

    def __deckMenu(self):
        """Present the deck menu to user until a valid selection is chosen"""
        menu = Menu.Menu("Deck")
        menu.addOption("P", "Print a card to the screen")
        menu.addOption("D", "Display the whole deck to the screen")
        menu.addOption("S", "Save the whole deck to a file")

        keepGoing = True
        while keepGoing:
            command = menu.show()
            if command == "P":
                self.__printCard()
            elif command == "D":
                print()
                self.__m_currentDeck.printDeck()
            elif command == "S":
                self.__saveDeck()
            elif command == "X":
                keepGoing = False

    def __printCard(self):
        """Command to print a single card"""
        cardToPrint = self.__getNumberInput("Id of card to print", 1, self.__m_currentDeck.getCardCount())
        if cardToPrint > 0:
            print()
            self.__m_currentDeck.printDeck(idx=cardToPrint)

    def __saveDeck(self):
        """Command to save a deck to a file"""
        fileName = self.__getStringInput("Enter output file name")
        if fileName != "":
            # TODO: open a file and pass to currentDeck.print()
            outputStream = open(fileName, 'w')
            self.__m_currentDeck.printDeck(outputStream)
            outputStream.close()
            print("Done!")

    def __getStringInput(self, prompt):
        stringInput = input(prompt + ": ")
        return stringInput

    def __getNumberInput(self, prompt, minimumValue, maximumValue):
        inputMessage = prompt + ": "
        numberInput = int(input(inputMessage))
        while numberInput < minimumValue or numberInput > maximumValue:
            inputError = "Invalid input. Enter a number between " + str(minimumValue) + " and " + str(maximumValue) + ": "
            numberInput = int(input(inputError))
        return numberInput
